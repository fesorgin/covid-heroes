<?php
	$as = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$alphabets = str_split($as);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>three.js webgl - orbit controls</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<link type="text/css" rel="stylesheet" href="main.css">
		<link type="text/css" rel="stylesheet" href="dock.css">
		<script type="text/javascript" src="js/jquery/jquery.min.js"></script>
	</head>

	<body>

		<div id="dockContainer">
			<div id="dockWrapper">
				<div class="cap left"></div>
				<ul class="osx-dock">
					<?php $l = 0; foreach($alphabets as $letter) { ?>
					<li class="letters letter_<?php echo $letter;?>">
						<a href="javascript:void();" title="View entries starting with <?php echo $letter; ?>"><?php echo $letter; ?></a>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>

		<script type="module">

			import * as THREE from './build/three.module.js';

			import { OrbitControls } from './jsm/controls/OrbitControls.js';

			var camera, controls, scene, renderer;

			var z = 8000;
			var l = 0;

            var xArray = [-200, 200, -200, 200];
            var yArray = [140, 140, -140, -140];
            var zArray = [300, 100, -100, -300];

            var backGeometry = new THREE.CircleGeometry( 100, 640 );
			var backMaterial = new THREE.ShaderMaterial({
				uniforms: {
					color1: {
						value: new THREE.Color("red")
					},
					color2: {
						value: new THREE.Color("purple")
					}
				},
				vertexShader: `
					varying vec2 vUv;

					void main() {
						vUv = uv;
						gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
					}
				`,
				fragmentShader: `
					uniform vec3 color1;
					uniform vec3 color2;
				
					varying vec2 vUv;
					
					void main() {
						gl_FragColor = vec4(mix(color1, color2, vUv.y), 1.0);
					}
				`,
				wireframe: true
			});

            var geometry = new THREE.CircleGeometry( 75, 480 );

			init();
			//render(); // remove when using next line for animation loop (requestAnimationFrame)
			animate();

			var lettersAdded = [];
			var letterPosition = [];
			var currentLetter = '';

			function init() {

				scene = new THREE.Scene();
				scene.background = new THREE.Color( 0xcccccc );
				//scene.fog = new THREE.FogExp2( 0xcccccc, 0.002 );

				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				document.body.appendChild( renderer.domElement );

				camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1000 );
				camera.position.set( 0, 0, 11150 );

				// controls

				controls = new OrbitControls( camera, renderer.domElement );
				controls.target = new THREE.Vector3( 0, 0, 1000);

				//controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)

				controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
				controls.dampingFactor = 0.001;

				controls.screenSpacePanning = false;

				controls.minDistance = 7500;
				controls.maxDistance = 10150;

				controls.maxPolarAngle = Math.PI / 2;

				controls.zoomSpeed = 0.05;

				// world
				loadObjects();

				controls.addEventListener('change', updateCanvas);

				// lights

				var light = new THREE.DirectionalLight( 0xffffff );
				light.position.set( 1, 1, 1 );
				scene.add( light );

				var light = new THREE.DirectionalLight( 0x002288 );
				light.position.set( - 1, - 1, - 1 );
				scene.add( light );

				var light = new THREE.AmbientLight( 0x222222 );
				scene.add( light );

				//

				window.addEventListener( 'resize', onWindowResize, false );
				window.addEventListener( 'click', onDocumentMouseDown, false );

			}

			function loadObjects() {

				var i = 0;
				$.getJSON("structure/data.json", function(json) {

					$.each(json.covid_heroes, function(index, data) {

						var letter = data.name.charAt(0);

						if(currentLetter != letter) {
							lettersAdded.push(letter);
							letterPosition.push(z);
							currentLetter = letter;

							$('.letter_'+letter).addClass('added');
						}

						var texture = new THREE.TextureLoader().load(
							"structure/"+(data.photo == null ? "no-image.png" : data.slug+"/"+data.photo)
						);

						var material = new THREE.MeshBasicMaterial({ map: texture });

						var position = i%4;

						var mesh = new THREE.Mesh( geometry, material );


						var nx = xArray[position] + Math.random() * 100;
						var ny = yArray[position] + Math.random() * 100;
						var nz = z + 100;

						mesh.position.x = nx;
						mesh.position.y = ny;
						mesh.position.z = z = nz;

						mesh.updateMatrix();
						mesh.matrixAutoUpdate = false;
						scene.add( mesh );

						var backMesh = new THREE.Mesh( backGeometry, backMaterial );

						backMesh.userData = data;

						backMesh.position.x = nx;
						backMesh.position.y = ny;
						backMesh.position.z = nz-1;

						backMesh.updateMatrix();
						//mesh.matrixAutoUpdate = false;
						scene.add( backMesh );

						//console.log("Object = x:"+mesh.position.x+", y:"+mesh.position.y+", z:"+mesh.position.z);

						i++;
					});

					console.log(lettersAdded);
					console.log(letterPosition);
				});
			}

			function updateCanvas() {

				var zoom = controls.target.distanceTo( controls.object.position ); console.log(zoom);


				for(var l = lettersAdded.length; l >= 0; l--) {
					if(zoom > letterPosition[l]) {
						$('.letters').removeClass('active');
						$('.letter_'+lettersAdded[l]).addClass('active');
						break;
					}
				}
			}

			function onWindowResize() {

				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();

				renderer.setSize( window.innerWidth, window.innerHeight );

			}

			function animate() {

				requestAnimationFrame( animate );

				controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true

				render();

			}

			function render() {

				renderer.render( scene, camera );

			}

			var raycaster = new THREE.Raycaster();
			var mouse = new THREE.Vector2();

			function onDocumentMouseDown( event ) {
				event.preventDefault();

				mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
				mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

				raycaster.setFromCamera( mouse, camera );

				var intersects = raycaster.intersectObjects( scene.children );

				if ( intersects.length > 0 && typeof(intersects[1]) != 'undefined') {
					var objectMesh = intersects[1].object;

					while (objectMesh.position.x < 400) {
						var d = objectMesh.position.x - 400;
						objectMesh.position.x += Math.min( 0.1, d );
					}

					console.log(objectMesh);
				}
			}

		</script>

	</body>
</html>
